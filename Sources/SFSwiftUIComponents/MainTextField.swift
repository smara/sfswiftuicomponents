//
//  MainTextField.swift
//  SFSwiftUIComponents
//
//  Created by Me on 24/07/20.
//

import SwiftUI

public struct MainTextField: View {
    @State var placeholder: String
    @Binding var text: String
    
    
    
    /// This is a textfield that is consisting to our design.
    /// - Parameters:
    ///   - placeholder: The text you see before you type anything
    ///   - text: Binding for the text that is typed into the field
    public init(placeholder: String, text: Binding<String>) {
        self._placeholder = State(initialValue: placeholder)
        self._text = text
    }
    
    public var body: some View {
        HStack{
            Image(systemName: "person")
                .foregroundColor(Color.blue)
            TextField(placeholder, text: $text)
                .font(.system(size:20, weight: .bold, design: .default))
                .foregroundColor(.blue)
        }
        .padding()
        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.blue, lineWidth: 2))
    }
}

struct MainTextField_Previews: PreviewProvider {
    static var previews: some View {
        MainTextField(placeholder: "Placeholder", text: .constant("Name"))
    }
}
