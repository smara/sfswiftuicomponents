//
//  MainTextFieldTests.swift
//  SFSwiftUIComponentsTests
//
//  Created by Me on 24/07/20.
//

import XCTest
import SwiftUI

class MainTextFieldTests: XCTestCase {
    @State var text: String = ""

    func testExample() throws {
        var body: some View {
            MainTextField(placeholder: "Testing", text: $text)
        }
//        XCTAssertEqual(SFSwiftUIComponents().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
