import XCTest

import SFSwiftUIComponentsTests

var tests = [XCTestCaseEntry]()
tests += SFSwiftUIComponentsTests.allTests()
XCTMain(tests)
